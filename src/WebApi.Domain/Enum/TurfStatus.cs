﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApi.Domain.Enum
{
    public enum TurfStatus
    {
        Active,
        InActive
    }
}
