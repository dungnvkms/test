﻿namespace WebApi.Domain
{
    public class BaseTime
    {
        public DateTime Created { get; set; }

        public DateTime Updated { get; set; }
    }
}
